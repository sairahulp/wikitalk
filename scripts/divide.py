#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""template.py: Description of what the module does."""

from optparse import OptionParser
import logging
import json
from random import shuffle

__author__ = "Rami Al-Rfou"
__email__ = "rmyeid@gmail.com"

LOG_FORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"
RATIOS = [0.0, 0.7, 0.8]

def main(options, args):
  fh = open(options.filename, 'r')
  list_ = json.load(fh)
  fh.close()
  shuffle(list_)
  ratios = zip(RATIOS, RATIOS[1:]+[1.0])
  size = float(len(list_))
  for ratio in ratios:
    start, end = ratio
    start_index = int(size*start)
    end_index = int(size*end)
    name = options.filename+'.'+str(end)
    fh = open(name, 'wb')
    logging.info("Dumping %s", name)
    json.dump(list_[start_index:end_index], fh)
    fh.close()

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option("-f", "--file", dest="filename", help="Input file")
  parser.add_option("-l", "--log", dest="log", help="log verbosity level",
                    default="INFO")
  (options, args) = parser.parse_args()

  numeric_level = getattr(logging, options.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOG_FORMAT)
  main(options, args)

